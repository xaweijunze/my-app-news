# MyAppNews

本项目基于 [Angular CLI](https://github.com/angular/angular-cli) 版本 9.0.0.


## 依赖安装

clone本项目之后，需要进行依赖安装 命令行运行： npm install.

## 运行测试

命令行运行： `ng serve`,可以通过本地`http://localhost:4200/`访问

## 线上地址

`http://www.forus616.cn:8081/newsPage`

## 注意事项

因为本项目开发的时候使用了nginx反向代理解决浏览器跨域问题，所以部分或者全部api接口可能会无法访问，请如果需要其他支持请联系QQ：772406641 询问
