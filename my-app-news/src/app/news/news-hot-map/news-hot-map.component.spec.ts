import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsHotMapComponent } from './news-hot-map.component';

describe('NewsHotMapComponent', () => {
  let component: NewsHotMapComponent;
  let fixture: ComponentFixture<NewsHotMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsHotMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsHotMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
