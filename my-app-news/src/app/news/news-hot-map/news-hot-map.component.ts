import {Component, OnInit} from '@angular/core';
import {EChartOption} from 'echarts';
import {HttpServiceService} from "../../core/http-service.service";
import * as echarts from 'echarts';

@Component({
  selector: 'app-news-hot-map',
  templateUrl: './news-hot-map.component.html',
  styleUrls: ['./news-hot-map.component.less']
})
export class NewsHotMapComponent implements OnInit {
  option: EChartOption;

  constructor(private httpService: HttpServiceService) {
  }

  ngOnInit(): void {
    this.pointHopMap();
    // this.httpService.httpService("./assets/json/cityNum.json",'')
    //   .subscribe((data:any)=>{
    //     console.log(data)
    //   })
  }

  fontSize(res) {
    let clientWidth =
      window.innerWidth ||
      document.documentElement.clientWidth ||
      document.body.clientWidth;
    if (!clientWidth) return;
    let fontSize = 100 * (clientWidth / 1920);
    return res * fontSize;
  }

  shiftToMapData(cityNumSelected, provinceData) {
    let mapData = [];
    // console.log(cityNumSelected[0][0][0])
    // console.log(provinceData[cityNumSelected[0][0][0]]);
    for (let i = 0; i < cityNumSelected.length; i++) {
      mapData[i] = []
      for (let j = 0; j < cityNumSelected[i].length; j++) {
        mapData[i].push([...provinceData[cityNumSelected[i][j][0]], cityNumSelected[i][j][2]]);
      }
    }
    return mapData;

  }

  shiftToBarData(cityNumSelected) {
    let barData = [];
    let categoryData = [];
    for (let i = 0; i < cityNumSelected.length; i++) {
      barData[i] = [];
      categoryData[i] = [];

      cityNumSelected[i].sort(function sortNumber(a, b) {
        return a[2] - b[2];
      })
      for (let j = 0; j < cityNumSelected[i].length; j++) {
        barData[i].push(cityNumSelected[i][j][0]);
        categoryData[i].push(cityNumSelected[i][j][2]);
      }

    }
    // console.log(barData, categoryData)
    return [barData, categoryData];

  }

  pointHopMap() {
    //颜色
    let colors = [
      [
        "#1DE9B6",
        "#F46E36",
        "#04B9FF",
        "#5DBD32",
        "#FFC809",
        "#FB95D5",
        "#BDA29A",
        "#6E7074",
        "#546570",
        "#C4CCD3"
      ],
      [
        "#37A2DA",
        "#67E0E3",
        "#32C5E9",
        "#9FE6B8",
        "#FFDB5C",
        "#FF9F7F",
        "#FB7293",
        "#E062AE",
        "#E690D1",
        "#E7BCF3",
        "#9D96F5",
        "#8378EA",
        "#8378EA"
      ],
      [
        "#DD6B66",
        "#759AA0",
        "#E69D87",
        "#8DC1A9",
        "#EA7E53",
        "#EEDD78",
        "#73A373",
        "#73B9BC",
        "#7289AB",
        "#91CA8C",
        "#F49F42"
      ]
    ];
    let maxNum = [150, 150, 100, 50];
    let colorIndex = 0;
    let month = [
      "2020/7-10",
      "2020/11-2021/01",
      "2021/02-2021/04",
      "2021/04-2021/06"
    ];
    let mapData = [[], [], [], [], []];
    let cityNumAll = [];
    let cityNumSelected = [[], [], [], [], []];

    /*柱子Y名称*/
    let categoryData = [];
    let barData = [];
    this.httpService.httpService('./assets/json/cityNum.json', '')
      .subscribe((res: any) => {
        let data = res.data;
        // console.log(data)
        let dataLength = data.length;
        for (let i = 0; i < dataLength; i += 3) {
          cityNumAll.push(data.splice(0, 3));
        }
        cityNumAll.forEach((item, index) => {
          cityNumSelected[index % 5].push(item);
        })
        // console.log(cityNumSelected)
        // console.log(cityNumAll);
        let [a, b] = this.shiftToBarData(cityNumSelected);
        barData = a;
        categoryData = b;
        this.httpService.httpService('./assets/json/province.json', '')
          .subscribe((res: any) => {
            // console.log(res)
            mapData = this.shiftToMapData(cityNumSelected, res);
            // console.log(mapData)
            this.httpService.httpService('./assets/json/china.json', '')
              .subscribe(geoJson => {
                echarts.registerMap('china', geoJson);
                this.option = {
                  timeline: {
                    data: month,
                    axisType: "category",
                    autoPlay: true,
                    playInterval: 6000,
                    left: "10%",
                    right: "10%",
                    bottom: "3%",
                    width: "80%",
                    label: {
                      normal: {
                        textStyle: {
                          color: "#ddd"
                        }
                      },
                      emphasis: {
                        textStyle: {
                          color: "#fff"
                        }
                      }
                    },
                    symbolSize: 10,
                    lineStyle: {
                      color: "#555"
                    },
                    checkpointStyle: {
                      borderColor: "#777",
                      borderWidth: 2
                    },
                    controlStyle: {
                      showNextBtn: true,
                      showPrevBtn: true,
                      normal: {
                        color: "#666",
                        borderColor: "#666"
                      },
                      emphasis: {
                        color: "#aaa",
                        borderColor: "#aaa"
                      }
                    }
                  },

                  //条形图
                  baseOption: {
                    animation: true,
                    animationDuration: 1000,
                    animationEasing: "cubicInOut",
                    animationDurationUpdate: 1000,
                    animationEasingUpdate: "cubicInOut",
                    grid: {
                      right: "5%",
                      top: "15%",
                      bottom: "10%",
                      width: "20%"
                    },
                    tooltip: {
                      trigger: "axis", // hover触发器
                      axisPointer: {
                        // 坐标轴指示器，坐标轴触发有效
                        type: "shadow", // 默认为直线，可选为：'line' | 'shadow'
                        shadowStyle: {
                          color: "rgba(150,150,150,0.1)" //hover颜色
                        }
                      }
                    },
                    //地图
                    geo: {
                      show: true,
                      map: "china",
                      roam: true,
                      zoom: 1,
                      center: [118.83531246, 34.0267395887],
                      label: {
                        emphasis: {
                          show: false
                        }
                      },
                      scaleLimit: {
                        min: 0.6,
                        max: 2
                      },
                      itemStyle: {
                        normal: {
                          borderColor: "rgba(147, 235, 248, 1)",
                          borderWidth: 1,
                          areaColor: {
                            type: "radial",
                            x: 0.5,
                            y: 0.5,
                            r: 0.8,
                            colorStops: [
                              {
                                offset: 0,
                                color: "rgba(147, 235, 248, 0)" // 0% 处的颜色
                              },
                              {
                                offset: 1,
                                color: "rgba(147, 235, 248, .2)" // 100% 处的颜色
                              }
                            ],
                            globalCoord: false // 缺省为 false
                          },
                          shadowColor: "rgba(255, 255, 255, 1)",
                          shadowOffsetX: -1,
                          shadowOffsetY: 1,
                          shadowBlur: 5
                        },
                        emphasis: {
                          areaColor: "#389BB7",
                          borderWidth: 0
                        }
                      }
                    },

                  },
                  options: []
                };
                for (let n = 0; n < month.length; n++) {
                  this.option.options.push({
                    backgroundColor: "#013954",
                    title: [
                      {
                        text: "新闻热度指数",
                        left: "30%",
                        top: "7%",
                        textStyle: {
                          color: "#fff",
                          fontSize: this.fontSize(0.28)
                        }
                      },
                      {
                        id: "statistic",
                        //month[n] +
                        text: "数据统计情况",
                        right: "5%",
                        top: "6%",
                        textStyle: {
                          color: "#fff",
                          fontSize: this.fontSize(0.25)
                        }
                      }
                    ],
                    xAxis: {
                      type: "value",
                      scale: true,
                      position: "top",
                      min: 0,
                      boundaryGap: false,
                      splitLine: {
                        show: false
                      },
                      axisLine: {
                        show: false
                      },
                      axisTick: {
                        show: false
                      },
                      axisLabel: {
                        margin: 2,
                        textStyle: {
                          color: "#aaa",
                          fontSize: this.fontSize(0.2)
                        }
                      }
                    },
                    yAxis: {
                      type: "category",
                      //  name: 'TOP 20',
                      nameGap: 16,
                      axisLine: {
                        show: true,
                        lineStyle: {
                          color: "#ddd"
                        }
                      },
                      axisTick: {
                        show: false,
                        lineStyle: {
                          color: "#ddd"
                        }
                      },
                      axisLabel: {
                        interval: 0,
                        textStyle: {
                          color: "#ddd",
                          fontSize: this.fontSize(0.18)
                        }
                      },
                      data: barData[n]
                    },

                    series: [
                      //地图
                      {
                        name: "热力图",
                        type: "heatmap",
                        map: "china",
                        geoIndex: 0,
                        aspectScale: 1, //长宽比
                        showLegendSymbol: false, // 存在legend时显示
                        coordinateSystem: "geo",
                        data: mapData[n]
                      },

                      //柱状图
                      {
                        zlevel: 1.5,
                        type: "bar",
                        symbol: "none",
                        itemStyle: {
                          normal: {
                            color: colors[colorIndex][n]
                          }
                        },
                        data: categoryData[n]
                      }
                    ],
                    visualMap: [
                      {
                        show: true,
                        left: "2%",
                        bottom: "2%",
                        max: maxNum[n],
                        min: 0,
                        z: 999,
                        calculable: false,
                        text: ["高", "低"],
                        inRange: {
                          color: ["#0033FF", "#FFFF00", "#FF3333"]
                        },
                        textStyle: {
                          color: "#2ecc71",
                          fontSize: this.fontSize(0.2)
                        },
                        seriesIndex: 0
                      }
                    ]
                  });
                }
              })
          })
      })
  }

}
