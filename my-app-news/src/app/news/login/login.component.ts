import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {HttpServiceService} from "../../core/http-service.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  validateForm!: FormGroup;
  submitForm(): void {
    // console.log(this.validateForm.value);
    if(this.validateForm.value.userName==null || this.validateForm.value.passWord == null ||
        this.validateForm.value.userName=='' || this.validateForm.value.passWord == ''
    ){
      alert('请输入账号密码！');
      return ;
    }
    this.httpService.httpService('http://81.70.233.131:3006/login',this.validateForm.value).subscribe((data:any)=>{
      // console.log(data)
      if(data.status == 200){
        window.localStorage.setItem('auth_token', JSON.stringify(data.data.token))
        window.localStorage.setItem('user_info', JSON.stringify(data.data.userName))
        this.router.navigate(['/home/list'])
      }else
      if(data.status==410){
        alert("账号密码错误，请重试");
      }

    });

    // for (const i in this.validateForm.controls) {
    //   if (this.validateForm.controls.hasOwnProperty(i)) {
    //     this.validateForm.controls[i].markAsDirty();
    //     this.validateForm.controls[i].updateValueAndValidity();
    //   }
    // }
  }

  constructor(private fb: FormBuilder, private httpService:HttpServiceService,private router:Router) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({
        userName: [null, [Validators.required]],
      passWord: [null, [Validators.required]],
      }
    );
  }

}
