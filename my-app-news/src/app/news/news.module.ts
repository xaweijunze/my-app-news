import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NewsListComponent} from './news-list/news-list.component';
import {NewsHotMapComponent} from './news-hot-map/news-hot-map.component';
import {EpidemicMapComponent} from './epidemic-map/epidemic-map.component';
import {NgxEchartsModule} from 'ngx-echarts';//引入部分
import {HttpClientModule} from "@angular/common/http";
import {FlexibleService} from '../core/flexible.service';
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {NgZorroAntdModule} from 'ng-zorro-antd';
import {NzTableModule} from 'ng-zorro-antd/table';

import {NzDividerModule} from 'ng-zorro-antd/divider';
import {NzFormModule} from 'ng-zorro-antd/form';
import {NzInputModule} from 'ng-zorro-antd/input';

import {NzModalModule} from 'ng-zorro-antd/modal';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {RouterModule} from "@angular/router";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [NewsListComponent,
    NewsHotMapComponent,
    EpidemicMapComponent,
    RegisterComponent,
    LoginComponent,
  ],
  exports: [
    NewsListComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,//声明
    NgxEchartsModule,
    ReactiveFormsModule,
    FormsModule,
    NzButtonModule,
    NzTableModule,
    NzDividerModule,
    NzFormModule,
    NzInputModule,
    NzModalModule,
    NgZorroAntdModule,
    RouterModule,
    BrowserAnimationsModule
  ],
  providers: [FlexibleService]
})
export class NewsModule {
}
