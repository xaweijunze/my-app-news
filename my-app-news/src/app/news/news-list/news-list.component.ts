import {Component, OnInit} from '@angular/core';
import {HttpServiceService} from "../../core/http-service.service";
import {migrateLegacyGlobalConfig} from "@angular/cli/utilities/config";
import {timeoutWith} from "rxjs/operators";

@Component({
    selector: 'app-news-list',
    templateUrl: './news-list.component.html',
    styleUrls: ['./news-list.component.less']
})
export class NewsListComponent implements OnInit {
    newsList = []
    offset = 0;
    isLoading = false;

    constructor(private httpService: HttpServiceService) {
    }

    ngOnInit(): void {
        this.getNewsList(this.offset);
        this.scroll()

    }

    // deBounce(fn, wait) {
    //     let timeOut = null;
    //     return function () {
    //         if (timeOut !== null) {
    //             clearTimeout(timeOut);
    //         }
    //         timeOut = setTimeout(fn, wait);
    //     }
    // }

    getNewsList(offset) {
        this.httpService.httpService(`https://i.news.qq.com/trpc.qqnews_web.kv_srv.kv_srv_http_proxy/list?sub_srv_id=24hours&srv_id=pc&offset=${offset}&limit=10&strategy=1&ext={"pool":["top"],"is_filter":7,"check_type":true}`, '')
            .subscribe((data: any) => {
                // console.log(data.data.list)
                this.newsList.push(...data.data.list);
                this.offset += 10;
                this.isLoading = false;

            })
    }

    scroll() {
        window.addEventListener('scroll', () => {
            let windowDoc = document.documentElement;
            // console.log(windowDoc.scrollHeight - Math.floor(windowDoc.scrollTop), windowDoc.clientHeight)
            if (windowDoc.scrollHeight - Math.floor(windowDoc.scrollTop) - 1 == windowDoc.clientHeight && !this.isLoading) {
                this.isLoading = true;
                this.getNewsList(this.offset);
            }
        })
    }
}
