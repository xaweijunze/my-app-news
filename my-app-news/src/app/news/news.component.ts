import {Component, OnInit} from '@angular/core';
import {FlexibleService} from '../core/flexible.service'

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.less']
})
export class NewsComponent implements OnInit {
  isActive = [
    false,
    false,
    false
  ];
  islogin= false;
  auth_token;
  userName;
  constructor(private flexible: FlexibleService) {
  }

  ngOnInit(): void {
    this.navPage();
    this.flexible.flexible(document.documentElement);
    this.auth_token = window.localStorage.getItem('auth_token');
    this.userName = window.localStorage.getItem('user_info');
    if (!this.auth_token) {
      this.islogin = false;
    } else {
      this.userName = this.userName.substring(1,this.userName.length-1)
      this.islogin = true;
    }
    // console.log(document.documentElement)
  }
  outline(){
    window.localStorage.removeItem('auth_token');
    window.localStorage.removeItem('user_info');
    this.islogin = false;

  }

  navPage() {
    // console.log(window.location.pathname)
    let pathName = window.location.href.split('/').reverse()[0];
    console.log(pathName)
    switch (pathName) {
      case "list":
        this.isActive = [true, false, false];
        break;
      case "hotmap":
        this.isActive = [false, true, false];
        break;
      case "epidemic":
        this.isActive = [false, false, true];
        break;
    }
  }

  // navPage(e){
  //   // console.log(e);
  //   if(e.path[1].getAttribute('class')!='active'){
  //     e.path[2].childNodes.forEach(index=>{
  //       index.classList.remove('active')
  //     })
  //     e.path[1].classList.add('active');
  //   }
  // }
}
