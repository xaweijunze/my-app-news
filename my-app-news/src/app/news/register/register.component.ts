import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpServiceService} from "../../core/http-service.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {
  validateForm!: FormGroup;
  // formGroup: '';
  isVisible=false;
  submitForm() {
    console.log(this.validateForm.value);
    if(this.validateForm.value.userName==null || this.validateForm.value.passWord == null ||
        this.validateForm.value.userName=='' || this.validateForm.value.passWord == ''
    ){
      alert('请输入账号密码！');
      return ;
    }
    this.httpService.httpService('http://81.70.233.131:3006/register', this.validateForm.value).subscribe((data: any) => {
      console.log(this.validateForm.value)
      console.log(data)
      if(data.status == 200){
        this.isVisible = true;
      }else if(data.staus == 409){
        alert('账号已存在，请重新注册账号！')
      }
    });
  }
  constructor(private fb: FormBuilder, private httpService: HttpServiceService,private router:Router) {
  }

  handleOk(): void {
    // console.log('Button ok clicked!');
    this.isVisible = false;
    this.router.navigate(['/login'])

  }
  ngOnInit(): void {
    this.validateForm = this.fb.group({
        userName: [null, [Validators.required]],
        passWord: [null, [Validators.required]],
      }
    );
  }


}
