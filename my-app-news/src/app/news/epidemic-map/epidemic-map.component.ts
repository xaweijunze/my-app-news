import {Component, OnInit} from '@angular/core';
import {EChartOption} from 'echarts';
import {HttpServiceService} from "../../core/http-service.service";
import * as echarts from 'echarts';

@Component({
  selector: 'app-epidemic-map',
  templateUrl: './epidemic-map.component.html',
  styleUrls: ['./epidemic-map.component.less']
})
export class EpidemicMapComponent implements OnInit {
  optionChina: EChartOption;
  optionWorld: EChartOption;
  option: EChartOption;
  abroadOrNot: string = "国外";
  optionFlag: boolean = true;
  dataChina;
  dataWorld;
  namesWorld;
  allOrNow: string = "今日新增";
  allOrNowFlag: boolean = false;

  constructor(private HttpService: HttpServiceService) {
  }


  ngOnInit(): void {

    this.getDataFrom163();

  }

  triggerAllOrNow() {
    if (this.optionFlag) {
      if (this.allOrNowFlag) {
        this.pointChina();
        this.allOrNowFlag = !this.allOrNowFlag;
      } else {
        this.pointChina();
        this.allOrNowFlag = !this.allOrNowFlag;
      }
    } else {
      if (this.allOrNowFlag) {
        this.pointWorld();
        this.option = this.optionWorld;
        this.allOrNowFlag = !this.allOrNowFlag;
      } else {
        this.pointWorld();
        this.option = this.optionWorld;
        this.allOrNowFlag = !this.allOrNowFlag;
      }
    }


  }

  // this.option = this.optionChina;


  trigger() {
    if (this.optionFlag) {
      this.option = this.optionWorld;
      this.optionFlag = !this.optionFlag;
      this.abroadOrNot = "国内"
    } else {
      this.optionFlag = !this.optionFlag;
      this.abroadOrNot = "国外"
      this.option = this.optionChina;


    }
  }

  test() {
    //翻译
    return this.HttpService.httpService('./assets/json/fanyi.json', '');
  }

  getDataFrom163() {
    this.HttpService.httpService('/api', '')
      .subscribe((data: any) => {
        let allData = data.data;
        this.dataChina = [];
        allData.areaTree[2].children.forEach((item) => {
          this.dataChina.push([item.name, item.today.confirm, item.total.confirm - item.total.dead - item.total.heal])
        })

        this.dataWorld = []
        allData.areaTree.forEach(item => {
          this.dataWorld.push([item.name, item.today.confirm, item.total.confirm - item.total.dead - item.total.heal])
        })
        let names = [];
        let str = ''
        this.dataWorld.forEach(item => {
          names.push(item[0]);
          str += item[0];
          str += ','
        });
        // console.log(str)
        this.test().subscribe((data: any) => {

          let ss = data.all;
          let namess = ('"' + ss.replace(/, /g, '","') + '"').split(","); //处理翻译的名字
          namess[0] = namess[0].substring(1, namess[0].length - 1)
          namess[namess.length - 1] = namess[namess.length - 1].substring(0, namess[0].length - 1)

          this.namesWorld = namess;
          // console.log(this.namesWorld);
          this.pointChina();
          this.pointWorld();

        });

      })
  }

  pointWorld() {
    let datas = [];
    let title = "";
    let min = 0;
    let max = 100;
    let values = [];

    title = "世界疫情现存地图";
    min = 1000;
    max = 100000;


    this.dataWorld.forEach((item, index) => {
      if (!item) return;
      if (!this.allOrNowFlag) {
        values.push(item[2]);
        if (min != 1000) {
          title = "世界疫情现存地图";
          min = 1000;
          max = 100000;
        }
      } else {
        values.push(item[1]);
        if (min != 10) {
          title = "世界疫情新增地图";
          min = 10;
          max = 1000;
        }
      }

      let sss = this.namesWorld[index];
      if (sss === "U.S.A") {
        sss = "United States";
      }
      datas.push({
        name: sss,
        value: values[index]
      });
    });


    this.HttpService.httpService('./assets/json/world.json', '')
      .subscribe(geoJson => {
        echarts.registerMap('world', geoJson);
        this.optionWorld = {
          title: {
            text: title,
            left: "center",
            top: 30

          },
          tooltip: {
            trigger: "item",
            showDelay: 0,
            transitionDuration: 0.2
          },
          visualMap: {
            left: "right",
            min: min,
            max: max,
            itemWidth: 15,
            itemHeight: 80,
            inRange: {
              //颜色梯度
              color: [
                "#f9f9fa",
                // "#bfbfc0",
                // "#74add1",
                // "#abd9e9",
                "#e0f3f8",
                "#ffffbf",
                "#fee090",
                "#fdae61",
                "#f46d43",
                "#d73027",
                "#a50026"
              ]
            },
            text: ["High", "Low"], // 文本，默认为数值文本
            calculable: true
          },
          toolbox: {
            show: true,
            //orient: 'vertical',
            left: "left",
            top: "top",
            feature: {
              dataView: {readOnly: false},
              restore: {},
              saveAsImage: {}
            }
          },
          series: [
            {
              name: "数据",
              type: "map",
              roam: true,
              map: "world",
              zoom: 2.5,// 改变这个值的大小就可以了
              emphasis: {
                label: {
                  show: true
                }
              },
              itemStyle: {
                normal: {
                  label: {
                    show: false
                  }
                }
              },
              data: datas
            }
          ]
        };
      })

  }

  pointChina() {
    let min = 0;
    let max = 100;
    let title = "地图";
    let data = [];
    let names = [];
    let values = [];

    // for (let i = 0; i < this.dataChina[1].length; i++) {
    //   names.push(this.dataChina[1][i]);
    //   let obj = JSON.parse(JSON.stringify(this.dataChina[2][i]));
    //   values.push(obj.confirmAdd);
    //   data.push({name: names[i], value: values[i]});
    //   // console.log(obj.nowConfirm)
    //   // console.log(this.dataChina[2][i])
    // }
    this.dataChina.forEach(item => {
      if (this.allOrNowFlag) {
        data.push({name: item[0], value: item[2]});
        title = "国内现存确诊地图";

      } else {
        data.push({name: item[0], value: item[1]});
        title = "国内较上日新增确诊地图";
      }
    })
    // console.log(names, values)

    this.HttpService.httpService('./assets/json/china.json', '')
      .subscribe(geoJson => {
        echarts.registerMap('china', geoJson);
        min = 0;
        max = 100;
        this.optionChina = {
          title: {
            text: title,
            left: "center",
            top: 30,

            textStyle:{
              //字体大小
            }

          },
          tooltip: {
            trigger: "item",
            showDelay: 0,
            transitionDuration: 0.2
          },
          visualMap: {
            left: "right",
            min: min,
            max: max,
            itemWidth: 15,
            itemHeight: 80,
            inRange: {
              //颜色梯度
              color: [
                "#f9f9fa",
                // "#bfbfc0",
                // "#74add1",
                // "#abd9e9",
                // "#e0f3f8",
                "#ffffbf",
                "#fee090",
                "#fdae61",
                "#f46d43",
                "#d73027",
                "#a50026"
              ]
            },
            text: ["High", "Low"], // 文本，默认为数值文本
            calculable: true
          },
          toolbox: {
            show: true,
            //orient: 'vertical',
            left: "left",
            top: "top",
            feature: {
              dataView: {readOnly: false},
              restore: {},
              saveAsImage: {}
            }
          },
          series: [
            {
              name: "数据",
              type: "map",
              roam: true,
              map: "china",
              zoom: 2.5,// 改变这个值的大小就可以了
              grid:{
                top: 80
              },
              emphasis: {
                label: {
                  show: true
                }
              },
              itemStyle: {
                normal: {
                  label: {
                    // show: true
                  }
                }
              },
              // 文本位置修正
              data: data
            }
          ]
        }
        this.option = this.optionChina;
      })


  }
}
