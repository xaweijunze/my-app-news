import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EpidemicMapComponent } from './epidemic-map.component';

describe('EpidemicMapComponent', () => {
  let component: EpidemicMapComponent;
  let fixture: ComponentFixture<EpidemicMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpidemicMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EpidemicMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
