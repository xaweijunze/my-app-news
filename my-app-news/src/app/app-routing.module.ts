import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NewsComponent} from "./news/news.component";
import {NewsListComponent} from "./news/news-list/news-list.component";
import {NewsHotMapComponent} from "./news/news-hot-map/news-hot-map.component";
import {EpidemicMapComponent} from "./news/epidemic-map/epidemic-map.component";
import {LoginComponent} from "./news/login/login.component";
import {RegisterComponent} from "./news/register/register.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home/list', // 当请求根路径的时候，跳转到 contacts 联系人组件
    pathMatch: 'full' // 必须完全匹配到路径的时候才做重定向
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },

  {
    path: 'home',
    component: NewsComponent,
    canActivate: [], // 在导航 contacts 之前会先进入路由守卫
    children:[
      {
        path: 'list',
        component: NewsListComponent,
      },
      {
        path: 'epidemic',
        component: EpidemicMapComponent,
      },
      {
        path: 'hotmap',
        component: NewsHotMapComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
