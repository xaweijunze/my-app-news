import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

  constructor(private http: HttpClient) {

  }

  httpService(url, data) {
    if (data == '')
      return this.http.get(url);

    return this.http.post(url, data);
  }


}
