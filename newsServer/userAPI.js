var express = require('express');
const route = express.Router();
const MysqlBase = require('./sqlPool')
const jwt = require('jwt-simple')
const md5 = require('blueimp-md5')
const moment = require('moment')

route.get('/', async (req, res) => {
    res.json({
        status: '200',
        msg: 'success',
        data: "success"
    })
}),
    //用户注册
    route.post('/register', async (req, res) => {
        console.log("get:/register")
        console.log(req.method)
        let userName = req.body.userName;
        req.body.passWord = md5(req.body.passWord)
        let passWord = req.body.passWord;
        let sql = 'insert into users (userName, passWord) values (?,?)';
        let sqlArr = [userName, passWord];
        MysqlBase(sql, sqlArr, (_err, data) => {
            if (_err) {
                if (_err.errno === 1062) {
                    res.status(409).json({
                        error: 'Validation Failed',
                        detail: [{
                            message: 'conflict',
                            field: 'userName',
                            code: 'conflict_field'
                        }]
                    })
                } else {
                    res.json({
                        status: '404',
                        msg: 'fail',
                        data: _err
                    })
                }

            } else {
                const results = data;
                res.json({
                    status: '200',
                    msg: 'success',
                    data: results
                })
            }
        })


    }),

//用户登录
    route.post('/login', async (req, res) => {
        console.log("get:/login")
        let userName = req.body.userName;
        let passWord = md5(req.body.passWord);
        console.log(userName, req.body.passWord, passWord)
        let sql = 'select userName,passWord,id from users where userName ="' + userName + '";';
        let sqlArr = [];
        MysqlBase(sql, sqlArr, (_err, data) => {
            if (_err) {
                res.json({
                    status: '404',
                    msg: 'fail',
                    data: _err
                })
            } else {
                let result = data[0];
                console.log(data.length)
                if (data.length != 1) {
                    res.json({
                        status: '410',
                        msg: 'fail',
                        data: 'userName wrong'
                    })
                    return
                } else if (passWord != result.passWord) {
                    res.json({
                        status: '410',
                        msg: 'fail',
                        data: 'password wrong'
                    })
                    return
                } else {
                    const token = jwt.encode({
                        iss: result.id, // 签发的用户 id 标识
                        exp: moment().add('days', 7).valueOf() // token 过期时间 7 天
                    }, 'itcast')

                    res.json({
                        status: '200',
                        msg: 'success',
                        data: {
                            token,
                            userName
                        }
                    })
                }

            }
        })

    }),
    module.exports = route;
